module bitbucket.org/tango-team/pyme-http-utils

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20190224122054-772b7c5f8a56
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.1
	github.com/prometheus/client_golang v1.10.0
	github.com/sirupsen/logrus v1.6.0
)
