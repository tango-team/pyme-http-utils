test-client:
	go test ./client
test-server:
	go test ./server
test-tenants:
	go test ./tenants
test: test-client test-server test-tenants