package server

import (
	"net/http"
	"strconv"
	"strings"
)

type QueryParams struct {
	Offset uint64
	Limit  uint64
	Search string
}

func ParseQueryParams(r *http.Request) QueryParams {
	offset, err := strconv.ParseUint(r.URL.Query().Get("offset"), 10, 64)
	if err != nil {
		offset = 0
	}

	limit, err := strconv.ParseUint(r.URL.Query().Get("limit"), 10, 64)
	if err != nil {
		limit = 100
	}

	search := r.URL.Query().Get("search")
	search = strings.TrimSpace(search)

	return QueryParams{
		Offset: offset,
		Limit:  limit,
		Search: search,
	}
}
