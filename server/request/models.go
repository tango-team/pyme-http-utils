package request

import (
	"net/http"
)

// Basic struct for request implementing Binder interface
type Request struct {
}

func (d *Request) Bind(r *http.Request) error {
	return nil
}
