package response

import (
	"net/http"

	"github.com/go-chi/render"
	log "github.com/sirupsen/logrus"
)

type RootResponse struct {
	Version interface{} `json:"version"`
	Status  string      `json:"status"`
}

func (response *RootResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, http.StatusOK)
	return nil
}

type Response struct {
	Data   interface{} `json:"data"`
	Errors []APIError  `json:"errors"`
	Status int         `json:"status"`
}

func (response *Response) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, response.Status)
	if response.Status == 500 {
		for _, e := range response.Errors {
			log.Warnf("API ERROR: %s %s %s", e.Reference, e.Field, e.Message)
		}
	}
	return nil
}

type APIError struct {
	Message   string `json:"message"`
	Field     string `json:"field"`
	Reference string `json:"ref"`
}
