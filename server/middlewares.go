package server

import "net/http"

func NewMiddlewareWrapper(middlewareFunc func(next http.Handler) http.Handler) func(next http.HandlerFunc) func(w http.ResponseWriter, r *http.Request) {
	return func(next http.HandlerFunc) func(w http.ResponseWriter, r *http.Request) {
		middleware := middlewareFunc(next)
		return func(w http.ResponseWriter, r *http.Request) {
			middleware.ServeHTTP(w, r)
		}
	}
}
