package server

import (
	"net/http"

	"bitbucket.org/tango-team/pyme-http-utils/server/response"
	"github.com/asaskevich/govalidator"
	"github.com/go-chi/render"
)

func CreateSuccessResponse(data interface{}, status ...int) response.Response {
	finalStatus := http.StatusOK
	if len(status) > 0 {
		finalStatus = status[0]
	}

	return response.Response{
		Data:   data,
		Errors: nil,
		Status: finalStatus,
	}
}

func CreateAPIErrorsResponse(errors []response.APIError, status ...int) response.Response {
	finalStatus := http.StatusInternalServerError
	if len(status) > 0 {
		finalStatus = status[0]
	}

	return response.Response{
		Data:   nil,
		Errors: errors,
		Status: finalStatus,
	}
}

func CreateAPIErrorResponse(err response.APIError, status ...int) response.Response {
	finalStatus := http.StatusInternalServerError
	if len(status) > 0 {
		finalStatus = status[0]
	}

	return response.Response{
		Data:   nil,
		Errors: []response.APIError{err},
		Status: finalStatus,
	}
}

func CreateErrorResponse(err error, status ...int) response.Response {
	finalStatus := http.StatusInternalServerError
	if len(status) > 0 {
		finalStatus = status[0]
	}

	return response.Response{
		Data:   nil,
		Errors: []response.APIError{{Message: err.Error()}},
		Status: finalStatus,
	}
}

func RespondSuccess(w http.ResponseWriter, r *http.Request, data interface{}, status ...int) {
	rsp := CreateSuccessResponse(data, status...)
	_ = render.Render(w, r, &rsp)
}

func RespondAPIError(w http.ResponseWriter, r *http.Request, err response.APIError, status ...int) {
	rsp := CreateAPIErrorResponse(err, status...)
	_ = render.Render(w, r, &rsp)
}

func RespondError(w http.ResponseWriter, r *http.Request, err error, status ...int) {
	rsp := CreateErrorResponse(err, status...)
	_ = render.Render(w, r, &rsp)
}

func RespondValidationErrors(w http.ResponseWriter, r *http.Request, err error, status ...int) {
	validationErrors, ok := err.(govalidator.Errors)
	if !ok {
		RespondError(w, r, err, status...)
		return
	}

	var apiErrors []response.APIError
	for _, fieldErrors := range validationErrors {
		fieldValidationErrors, ok := fieldErrors.(govalidator.Errors)
		if !ok {
			fieldValidationError, ok := fieldErrors.(govalidator.Error)
			if ok {
				apiError := response.APIError{
					Field:   fieldValidationError.Name,
					Message: fieldValidationError.Err.Error(),
				}
				apiErrors = append(apiErrors, apiError)
				continue
			}

			apiErrors = append(apiErrors, response.APIError{Message: fieldErrors.Error()})
			continue
		}

		for _, fieldError := range fieldValidationErrors {
			fieldValidationError, ok := fieldError.(govalidator.Error)
			if !ok {
				apiErrors = append(apiErrors, response.APIError{Message: fieldError.Error()})
				continue
			}

			apiError := response.APIError{
				Field:   fieldValidationError.Name,
				Message: fieldValidationError.Err.Error(),
			}
			apiErrors = append(apiErrors, apiError)
		}
	}

	rsp := CreateAPIErrorsResponse(apiErrors, status...)
	_ = render.Render(w, r, &rsp)
}
