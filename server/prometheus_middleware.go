package server

import (
	"github.com/go-chi/chi"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"net/http"
	"strconv"
	"time"
)

type StatusRecorder struct {
	http.ResponseWriter
	Status int
}

func (r *StatusRecorder) WriteHeader(status int) {
	r.Status = status
	r.ResponseWriter.WriteHeader(status)
}

func NewPrometheusMiddleware(service string) func(next http.Handler) http.Handler {
	var totalRequests = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name:        "http_requests_total",
			Help:        "Number of http requests processed, partitioned by status code",
			ConstLabels: prometheus.Labels{"service": service},
		},
		[]string{"status", "method", "path"},
	)

	var httpDuration = promauto.NewSummaryVec(
		prometheus.SummaryOpts{
			Name:        "http_request_duration_seconds",
			Help:        "Handlers latency distributions.",
			Objectives:  map[float64]float64{0.5: 0.05, 0.90: 0.01, 0.95: 0.01, 0.99: 0.001, 0.9999: 0.00001},
			ConstLabels: prometheus.Labels{"service": service},
		},
		[]string{"status", "method", "path"},
	)

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			recorder := &StatusRecorder{ResponseWriter: w, Status: 200}
			next.ServeHTTP(recorder, r)
			end := time.Now()

			rCtx := chi.RouteContext(r.Context())
			path := rCtx.RoutePattern()
			durationInSeconds := float64(end.Sub(start).Microseconds()) / float64(time.Second.Microseconds())

			totalRequests.WithLabelValues(strconv.Itoa(recorder.Status), r.Method, path).Inc()
			httpDuration.WithLabelValues(strconv.Itoa(recorder.Status), r.Method, path).Observe(durationInSeconds)
		})
	}
}
