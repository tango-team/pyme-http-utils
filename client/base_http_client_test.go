package client_test

import (
	"context"
	"net/http"
	"testing"

	"bitbucket.org/tango-team/pyme-http-utils/client"
)

type UserAgentResponse struct {
	UserAgent string `json:"user-agent"`
}

func TestError(t *testing.T) {
	var apiError client.APIError = client.APIError{
		Message:   "Hello",
		Field:     "World",
		Reference: "Error",
	}

	want := "Error.World: Hello"

	if got := apiError.Error(); got != want {
		t.Errorf("Error() = %q, want %q", got, want)
	}
}

func TestExecuteRequestAndGetResponse(t *testing.T) {
	var client client.BaseClient = client.BaseClient{
		BaseURL: "https://httpbin.org/",
		Client:  &http.Client{},
	}

	want := UserAgentResponse{
		UserAgent: "Go-http-client/2.0",
	}
	var got UserAgentResponse

	error := client.ExecuteRequestAndGetResponse(context.Background(), "GET", client.BuildURL("user-agent"), nil, nil, nil, &got)

	if error != nil {
		t.Error()
	}

	if got.UserAgent != want.UserAgent {
		t.Errorf("BuildURL() = %q, want %q", got.UserAgent, want.UserAgent)
	}
}

func TestBuildURL(t *testing.T) {
	var client client.BaseClient = client.BaseClient{
		BaseURL: "http://example.com/",
		Client:  &http.Client{},
	}

	want := "http://example.com/hello"

	if got := client.BuildURL("hello"); got != want {
		t.Errorf("BuildURL() = %q, want %q", got, want)
	}
}
