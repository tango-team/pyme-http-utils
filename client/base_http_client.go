package client

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"bitbucket.org/tango-team/pyme-http-utils/tenants"

	log "github.com/sirupsen/logrus"
)

var (
	ErrNotFound     = errors.New("not found")
	ErrUnauthorized = errors.New("unauthorized")
	ErrBadRequest   = errors.New("bad request")
	ErrUnknown      = errors.New("unknown error")
)

type BaseClient struct {
	BaseURL     string
	Client      *http.Client
	Headers     Headers
	QueryParams Query
}

type Query map[string]string
type Headers map[string]string
type Response struct {
	Data   interface{} `json:"data"`
	Errors []APIError  `json:"errors"`
	Status int         `json:"status"`
}
type APIError struct {
	Message   string `json:"message"`
	Field     string `json:"field"`
	Reference string `json:"ref"`
}

func (e APIError) Error() string {
	if len(e.Reference) > 0 && len(e.Field) > 0 {
		return fmt.Sprintf("%s.%s: %s", e.Reference, e.Field, e.Message)
	}

	if len(e.Field) > 0 {
		return fmt.Sprintf("%s: %s", e.Field, e.Message)
	}

	return e.Message
}

func (c *BaseClient) ExecuteRequestAndGetStdResponse(ctx context.Context, method string, url string, queryParams Query, headers Headers, data interface{}, result interface{}) error {
	response := Response{Data: result}
	err := c.ExecuteRequestAndGetResponse(ctx, method, url, queryParams, headers, data, &response)
	if err != nil {
		return err
	}

	if len(response.Errors) > 0 {
		msg := ""
		for _, err := range response.Errors {
			if len(msg) == 0 {
				msg = err.Error()
				continue
			}
			msg = fmt.Sprintf("%s, %s", msg, err.Error())
		}
		return errors.New(msg)
	}

	return nil
}

func (c *BaseClient) ExecuteRequestAndGetResponse(ctx context.Context, method string, url string, queryParams Query, headers Headers, data interface{}, result interface{}) error {
	rsp, responseBody, err := c.ExecuteRequest(ctx, method, url, queryParams, headers, data)
	if err != nil {
		return err
	}

	if rsp != nil {
		if rsp.StatusCode == http.StatusNotFound {
			return ErrNotFound
		}

		if rsp.StatusCode == http.StatusUnauthorized {
			return ErrUnauthorized
		}

		if rsp.StatusCode == http.StatusBadRequest || rsp.StatusCode == http.StatusUnprocessableEntity {
			return fmt.Errorf("%w : %s", ErrBadRequest, string(responseBody))
		}

		if rsp.StatusCode > http.StatusBadRequest {
			return fmt.Errorf("%w : %s", ErrUnknown, string(responseBody))
		}

		if rsp.StatusCode == http.StatusNoContent {
			return nil
		}
	}

	if result != nil {
		err = json.Unmarshal(responseBody, result)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *BaseClient) ExecuteRequest(ctx context.Context, method string, requestURL string, queryParams Query, headers Headers, data interface{}) (*http.Response, []byte, error) {
	var body io.Reader

	if data != nil {
		if val, ok := c.Headers["Content-Type"]; ok {
			if strings.Contains(val, "application/x-www-form-urlencoded") {
				values := data.(url.Values)
				body = bytes.NewBufferString(values.Encode())
			} else {
				jsonData, err := json.Marshal(data)
				if err != nil {
					return nil, nil, err
				}
				body = bytes.NewBuffer(jsonData)
				c.Headers["Content-Type"] = "application/json"
			}
		} else {
			jsonData, err := json.Marshal(data)
			if err != nil {
				return nil, nil, err
			}
			body = bytes.NewBuffer(jsonData)
			c.Headers["Content-Type"] = "application/json"
		}
	}

	req, err := http.NewRequestWithContext(ctx, method, requestURL, body)
	if err != nil {
		return nil, nil, err
	}

	tenantID, ok := tenants.GetTenantID(ctx)
	if ok {
		req.Header.Add(tenants.TenantIDHeader, tenantID)
	}

	for k, v := range c.Headers {
		req.Header.Add(k, v)
	}
	for k, v := range headers {
		req.Header.Add(k, v)
	}

	query := req.URL.Query()
	for k, v := range c.QueryParams {
		query.Add(k, v)
	}
	for k, v := range queryParams {
		query.Add(k, v)
	}
	req.URL.RawQuery = query.Encode()

	rsp, err := c.Client.Do(req)
	if err != nil {
		return rsp, nil, err
	}

	rspBody, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return rsp, nil, err
	}

	log.WithFields(log.Fields{
		"method":     method,
		"requestURL": req.URL.String(),
		"body":       data,
		"response":   string(rspBody),
	}).Debug("HTTP CLIENT REQUEST")

	return rsp, rspBody, nil
}

func (c *BaseClient) BuildURL(url string, args ...interface{}) string {
	baseURL := strings.TrimSuffix(c.BaseURL, "/")
	url = strings.TrimPrefix(url, "/")
	urlTemplate := fmt.Sprintf("%s/%s", baseURL, url)
	return fmt.Sprintf(urlTemplate, args...)
}

func (c *BaseClient) BuildURLClone2(url string, args ...interface{}) string {
	baseURL := strings.TrimSuffix(c.BaseURL, "/")
	url = strings.TrimPrefix(url, "/")
	urlTemplate := fmt.Sprintf("%s/%s", baseURL, url)
	return fmt.Sprintf(urlTemplate, args...)
}
