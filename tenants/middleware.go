package tenants

import (
	"bitbucket.org/tango-team/pyme-http-utils/server"
	"fmt"
	"net/http"
)

// NewTenantsMiddleware will build a handler to manage tenant identification using the X-TENANT-ID header
func NewTenantsMiddleware(acceptedTenants []string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			tenantID := r.Header.Get(TenantIDHeader)
			if len(acceptedTenants) > 0 && !contains(acceptedTenants, tenantID) {
				server.RespondError(w, r, fmt.Errorf("invalid tenant id"), http.StatusBadRequest)
				return
			}

			next.ServeHTTP(w, r.WithContext(SetTenantID(r.Context(), tenantID)))
		})
	}
}

func contains(items []string, value string) bool {
	for _, item := range items {
		if item == value {
			return true
		}
	}
	return false
}
