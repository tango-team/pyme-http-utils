package tenants

import (
	"context"
)

const TenantIDContextKey = "tenant_id"
const TenantIDHeader = "X-TENANT-ID"

func GetTenantID(ctx context.Context) (string, bool) {
	tenant, ok := ctx.Value(TenantIDContextKey).(string)
	return tenant, ok
}

func SetTenantID(ctx context.Context, tenantID string) context.Context {
	return context.WithValue(ctx, TenantIDContextKey, tenantID)
}
