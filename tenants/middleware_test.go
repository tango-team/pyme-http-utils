package tenants_test

import (
	"bitbucket.org/tango-team/pyme-http-utils/tenants"
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewTenantsMiddlewareEmptyAcceptedTenantsSuccess(t *testing.T) {
	var acceptedTenants []string
	expectedStatus := http.StatusOK
	middleware := tenants.NewTenantsMiddleware(acceptedTenants)(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {}))
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/", nil)

	middleware.ServeHTTP(w, r)

	if w.Code != expectedStatus {
		t.Errorf("Wrong middleware behaviour, expected status %d got %d", expectedStatus, w.Code)
	}
}

func TestNewTenantsMiddlewareNonEmptyAcceptedTenantsSuccessStatus(t *testing.T) {
	var expectedTenantID = "test-tenant-id"
	var acceptedTenants = []string{expectedTenantID}
	expectedStatus := http.StatusOK
	middleware := tenants.NewTenantsMiddleware(acceptedTenants)(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {}))
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/", nil)
	r.Header.Set(tenants.TenantIDHeader, expectedTenantID)

	middleware.ServeHTTP(w, r)

	if w.Code != expectedStatus {
		t.Errorf("Wrong middleware behaviour, expected status %d got %d", expectedStatus, w.Code)
	}
}

func TestNewTenantsMiddlewareNonEmptyAcceptedTenantsSuccessContextTenantId(t *testing.T) {
	var leakedContext context.Context
	var expectedTenantID = "test-tenant-id"
	var acceptedTenants = []string{expectedTenantID}
	middleware := tenants.NewTenantsMiddleware(acceptedTenants)(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		leakedContext = request.Context()
	}))
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/", nil)
	r.Header.Set(tenants.TenantIDHeader, expectedTenantID)

	middleware.ServeHTTP(w, r)

	receivedTenantID, _ := leakedContext.Value(tenants.TenantIDContextKey).(string)
	if receivedTenantID != expectedTenantID {
		t.Errorf("Wrong middleware behaviour, expected context tenant id \"%s\" got \"%s\"", expectedTenantID, receivedTenantID)
	}
}

func TestNewTenantsMiddlewareNonEmptyAcceptedTenantsFail(t *testing.T) {
	var acceptedTenants = []string{"test-tenant-id"}
	expectedStatus := http.StatusBadRequest
	middleware := tenants.NewTenantsMiddleware(acceptedTenants)(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {}))
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/", nil)

	middleware.ServeHTTP(w, r)

	if w.Code != expectedStatus {
		t.Errorf("Wrong middleware behaviour, expected status %d got %d", expectedStatus, w.Code)
	}
}
